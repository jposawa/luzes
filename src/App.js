import React, {Component} from 'react';
// import Routes from './routes.js'
import "./styles.css";
import luzes from "./util/luzes.json";

class App extends Component
{
  constructor(props)
  {
    super(props);
    this.state = {
      grau:"aprendiz",
    };
  }

  componentDidMount()
  {
    this.GeraBotoes();
    console.log(document.documentElement.clientWidth);
  }

  componentDidUpdate(prevProps, prevState)
  {
    const {state} = this;
    if(prevState !== null && prevState !== undefined)
    {
      // console.log(prevState.grau);
      if(prevState.grau != state.grau)
      {
        this.GeraBotoes();
      }
    }
    // console.log(window.innerWidth);
  }

  ReorganizaBotoes = () =>
  {
    const {state} = this;
    const {grau} = state;
    let larguraTela = document.documentElement.clientWidth;

    console.log(luzes[grau]);

    if(larguraTela < 560)
    {
      let numLinha = 0;

      for(let i = 0; i < luzes[grau].length; i++)
      {
        if(i % 3 === 0)
        {
          numLinha++;
        }
        
        luzes[grau][i].linha = numLinha;
      }
    }
  }

  GeraBotoes = () =>
  {
    const {state} = this;
    let {grau} = state;
    if(grau === undefined || grau === null || grau === "")
    {
      grau = "aprendiz";
    }
    const linhasLuzes = {};
    const quadroLuzes = document.getElementById("quadroLuzes");
    let htmlQuadroLuzes = "";

    this.ReorganizaBotoes();

    let numero = 0;
    luzes[grau].forEach(luz => {
      //ISSO CRIA DUAS VARIÁVEIS "nome" e "linha" QUE SERIAM O MESMO QUE "luz.nome" e "luz.linha". CHAMA DE DESCONSTRUIR O OBJETO NO JAVASCRIPT
      let {nome,linha} = luz;
      // console.log(nome);
      //AGORA CRIAR O BOTÃO
      let botao = "<button id=\"" + numero++ + "\" class=\"botaoPainel\" type=\"button\">";
      botao += nome;
      botao += "</button>";

      if(linhasLuzes[linha] === undefined || linhasLuzes[linha] === null)
      {
        linhasLuzes[linha] = "";
      }

      linhasLuzes[linha] += botao;
    })

    Object.values(linhasLuzes).forEach(linha => {
      htmlQuadroLuzes += "<p>";
      htmlQuadroLuzes += linha;
      htmlQuadroLuzes += "</p>";
    })

    // console.log(luzes[grau]);
    // console.log(linhasLuzes);

    quadroLuzes.innerHTML = htmlQuadroLuzes;
    // console.log(grau);
    this.setState({listaLuzes:luzes[grau],grau:grau});
  }

  CliqueBotao = (evento) =>
  {
    const {state} = this;
    const {listaLuzes} = state;
    const campo = evento.target;
    console.log(campo);
    console.log(campo.id);
    console.log(listaLuzes[campo.id]);
    console.log(campo.type);

    if(campo.type === "button")
    {
      campo.classList.toggle("ativado");
    }
  }

  render()
  {
    return(
      <div className="App">
        <div id="containerQuadroLuzes">
          <div id="quadroLuzes" onClick={this.CliqueBotao}></div>
        </div>
      </div> 
    );
  }
}

export default App;